use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct QueryParams {
    x: i32,
    y: i32,
}

#[derive(Serialize)]
struct Response {
    total: i32,
}

async fn add(query: web::Query<QueryParams>) -> impl Responder {
    let total = query.x + query.y;
    let resp = Response { total };
    HttpResponse::Ok().json(resp)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(add))
    })
    .bind("localhost:9000")?
    .run()
    .await
}
